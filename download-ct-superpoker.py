#!coding: utf-8
import os
import requests
from BeautifulSoup import BeautifulSoup as bs
from datetime import datetime
import json
import re
from slugify import slugify

def log(txt):
    now = datetime.now()
    print "[%s] %s" % (now, txt)

URL = "http://www.ctsuperpoker.com/videos.php?pagina=%s&limite_inicio=%s"
PAGINA = 1
LIMITE = 0

log("Logando...")
session = requests.Session()
login = session.post(
            'http://www.ctsuperpoker.com/login.php', 
            data={'login': 'luizfellipeboldrini@uol.com.br', 'senha': 'Boldrini24'},
            allow_redirects=False
        )

links = []

if os.path.exists('/tmp/ct.json'):
    with open('/tmp/ct.json') as handle:
        try:
            links = json.loads(handle.read())
        except:
            pass

if not links:
    while PAGINA <= 53:

        url = URL % (PAGINA, LIMITE)
        log("Consultando %s" % url)
        req = session.get(url, timeout=30)

        data = bs(req.content)
        for a in data.findAll('a', attrs={'class': 'assista'}):

            link = a.get('href')
            links.append(link)        

        LIMITE += 5
        PAGINA += 1

    with open('/tmp/ct.json', 'w') as output:
        output.write(json.dumps(links, indent=4))

videos = {}
if os.path.exists('/tmp/ct2.json'):
    with open('/tmp/ct2.json') as handle:
        try:
            videos = json.loads(handle.read())
        except:
            pass

if not videos:
    for link in links:

        attrs = {}
        for attr in link.split("?")[-1].split("&"):
            key, value = attr.split("=")
            attrs[key] = value

        id = int(attrs['video'])
        dic = {}

        log("Consultando video %s..." % link)
        req = session.get("http://www.ctsuperpoker.com/%s" % link, timeout=30)
        data = bs(req.content)

        title = data.find(attrs={'id': 'tit-video'})
        dic['title'] = title.text
        dic['mp4'] = re.search(r'htt.*mp4', req.content).group().strip()

        videos[id] = dic

    with open('/tmp/ct2.json', 'w') as output:
        output.write(json.dumps(videos, indent=4))

for video in videos:

    id = video
    video = videos[video]
    
    link = video['mp4']
    title = video['title']
    
    log("Consultando video '%s'..." % title)

    old_file = link.split("/")[-1]
    new_file = "%s-%s.mp4" % (id, slugify(title))

    os.system("wget %s" % link)
    os.system("mv %s %s" % (old_file, new_file))