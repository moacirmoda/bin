rm -rf /tmp/arcon-interface

echo Trazendo do interface do servidor de dev
scp -r arcondev:~/git/arcon-base/arcon-interface/ /tmp > /dev/null 2>&1 
echo Levando para o servidor $1
scp -r /tmp/arcon-interface/* $1:/opt/arcon-base/interface > /dev/null 2>&1

rm -rf /tmp/arcon-interface