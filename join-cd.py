#!coding: utf-8

import os, sys

if len(sys.argv) == 1:
	sys.exit(1)

path, = sys.argv[1:]
new_file = "/tmp/file.mp3"
try:
	os.remove(new_file)
except:
	pass

for root, dirs, files in os.walk(path):
	for file in files:
		if file.endswith(".mp3"):
			file = os.path.join(root, file)
			file = str(file).replace(" ", "\ ")
			os.system("cat %s >> %s" % (file, new_file))