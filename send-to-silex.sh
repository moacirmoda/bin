#!/bin/bash

pathfile=$1
file=$(basename $pathfile)

rsync -rv $pathfile szdev2:/tmp
ssh szdev2 "scp -r -p 9317 /tmp/$file moacir.moda@gatekeeper3.bireme.br:/tmp"
ssh szdev2 ssh gatekeeper scp -r /tmp/$file silex:/tmp