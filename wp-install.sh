#!/bin/bash

if [ $# == 0 ] 
then
	exit 1;
fi

name=${1}

echo "Criando estrutura e vhost.."
/home/moa/bin/make-vhost.sh $name

echo "Criando banco de dados.."
mysql -uroot -pmoa00moa -e "DROP DATABASE IF EXISTS $name"
mysql -uroot -pmoa00moa -e "CREATE DATABASE $name DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"

echo "Baixando WP.."
cd /home/moa/project/$name/htdocs
wp core download --locale=pt_BR

echo "Configurando WP.."
wp core config --dbname=$name --dbuser=root --dbpass=moa00moa --locale=pt_BR

echo "Instalando WP.."
wp core install --url=http://www.$name.dev --title=$name --admin_user=admin --admin_password=admin --admin_email=admin@$name.dev
