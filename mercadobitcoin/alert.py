#!/usr/bin/env python
#!coding: utf-8

import os, requests, json
from libs import open_data, save_data, log, file_with_message
from datetime import datetime

# este foi o valor da minha primeira compra
valor_de_compra_base  = 3250.00

# esta é a porcentagem que deve vender, relativo a valor de compra base
porcentagem_de_venda  = 5
# este é a porcentagem que deve comprar, relativo a compra base
porcentagem_de_compra = 10

now = datetime.now()

data = open_data()
url = "https://www.mercadobitcoin.com.br/api/ticker/"

r = json.loads(requests.get(url).text)
valor_final = float(r['ticker']['sell'])

if not 'valor_inicial' in data:
	data['valor_inicial'] = valor_de_compra_base
	save_data(data)

if now.hour == 0 and now.minute == 0:
	data['valor_inicial'] = valor_final
	save_data(data)

valor_inicial = data['valor_inicial']
if 'valor_inicial' in data:
	percent_sell = (porcentagem_de_venda * 0.01) + 1.0
	percent_buy  = 1.0 - (porcentagem_de_compra * 0.01)

	wish_sell = valor_inicial * percent_sell
	wish_buy = valor_inicial * percent_buy
	
	percent = ((valor_final * 100) / valor_inicial) - 100;
	percent = "%.2f" % percent + "%"
	
	log("base: R$ %.2f | atual: R$ %.2f | atual: %s | compra esperada: R$ %.2f | venda esperada: R$ %.2f" % 
		(valor_de_compra_base, valor_final, percent, wish_buy, wish_sell))

#	if valor_final >= wish_sell:
#		print "Enviando email.."
#		message = file_with_message("SUBIU EM "+percent+"%! VENDA!!")
#		os.system("/home/moa/bin/sendmail moa.moda@gmail.com %s" % message)
#
#	if valor_final <= wish_buy:
#		print "Enviando email.."
#		message = file_with_message("DESCEU EM "+percent+"%! COMPRE!!")
#		os.system("/home/moa/bin/sendmail moa.moda@gmail.com %s" % message)
