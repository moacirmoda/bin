#!coding: utf-8

import os, json
from datetime import datetime

def open_data():
	file = "/home/moa/bin/mercadobitcoin/data.json"

	if os.path.exists(file):
		with open(file) as handle:
			data = handle.read()
			if not data:
				return {}
			return json.loads(data)
	else:
		with open(file, 'w') as handle:
			handle.write("{}")
		return {}

def save_data(data):
	file = "/home/moa/bin/mercadobitcoin/data.json"
	with open(file, 'w') as handle:
		json.dump(data, handle)

def log(message):
	message = "[%s] %s\n" % (datetime.now(), message)
	with open('/tmp/bitcoin.log', 'a') as handle:
		handle.write(message)

def file_with_message(message):
	file = '/tmp/tmpfile'
	with open(file, 'w') as handle:
		handle.write(message)
	return file