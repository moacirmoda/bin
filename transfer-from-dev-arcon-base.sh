rm -rf /tmp/arcon-base

echo Trazendo do arcon-base do servidor de dev
scp -r arcondev:~/git/arcon-base/ /tmp > /dev/null 2>&1 
rm -rf /tmp/arcon-base/arcon-interface

echo Levando para o servidor $1
scp -r /tmp/arcon-base/* $1:/opt/arcon-base/ > /dev/null 2>&1

rm -rf /tmp/arcon-base