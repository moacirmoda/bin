#!/usr/bin/env python

import json
import sys

if len(sys.argv) > 1:
  
  file = sys.argv[1]
  
  with open(file) as handle:
    content = json.load(handle)
  
  print json.dumps(content, indent=2)
