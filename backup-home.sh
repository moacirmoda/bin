#!/bin/bash

HOME='/home/moa'
BACKUP_DESTINY='/media/win7/backup'
NOW=`date +%Y%m%d` 
BACKUP_PREFIX='backup_moa_'

backup_name="${BACKUP_PREFIX}${NOW}.tar.gz"

echo "* doing backup.."
tar cfzp /tmp/${backup_name} ${HOME}

echo "* removing old backup from destiny.."
rm ${BACKUP_PREFIX}*

echo "* coping new backup to destiny.."
cp /tmp/${backup_name} ${BACKUP_DESTINY}

echo "* removing temporary files.."
rm /tmp/${backup_name}

echo "* done!"
