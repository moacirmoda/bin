#!/usr/bin/env python
#! coding: utf-8

import json, random

with open("/home/moa/bin/ponto/data.json") as handle:
    data = json.load(handle)

almoco = [
    ['12:22', '13:21'],
    ['12:11', '13:12'],
    ['12:21','13:22'],
    ['12:44','13:41'],
    ['12:10','13:20'],
    ['11:55','12:45'],
    ['12:55','13:45'],
]

new_data = {}
for key, value in data.items():
    day, month, year = key.split("/")

    if len(day) < 2: day = "0%s" % day
    if len(month) < 2: month = "0%s" % month

    new_data["%s%s%s" % (year, month, day)] = value

keys = new_data.keys()
keys.sort()
for item in keys:
    print item + "\t",
    
    first = True
    for item2 in new_data[item]:
        print item2 + "\t", 

        if first:
            random.shuffle(almoco)
            print "%s\t%s\t" % (almoco[0][0], almoco[0][1]), 
            first = False
    print  

lines = ""
for key in keys:
    lines += '"%s";' % key
    for item in new_data[key]:
        lines += '"%s";' % item
    lines += "\n"

with open('/tmp/ponto.csv', 'w') as handle:
    handle.write(lines)

