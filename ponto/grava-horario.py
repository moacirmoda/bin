#!/usr/bin/env python
#! coding: utf-8
from datetime import datetime
import json
import os

datafile = os.path.join('/home/moa/bin/ponto', 'data.json')

# abre o arquivo de ponto
try:
    with open(datafile) as handle:
       data = json.load(handle)
except:
    data = {}


current_day = datetime.now().strftime("%Y%m%d")
current_time = datetime.now().strftime("%H:%M")

# verifica se já existe o dia, se não cria
if current_day in data.keys():

    # verifica se não é o mesmo horário
    if not current_time in data[current_day]:

        # # verifica se já não existem duas datas
        # if len(data[current_day]) < 2:
        #     data[current_day].append(current_time)
else:
    data[current_day] = [current_time]

with open(datafile, 'w') as handle:
    json.dump(data, handle, indent=4)
