#!/bin/bash

BKP_PATH="/home/moa/bkp"
mkdir -p $BKP_PATH

PHONE_ORIG_PATH="/home/moa/Dropbox/Camera*"
IMG_PATH="/home/moa/Dropbox/Imagens"

cur_date=`date +%Y%m%d%H%M`

tar cvfzp ${BKP_PATH}/fotos-videos-${cur_date}.tar.gz ${PHONE_ORIG_PATH} ${IMG_PATH}
logger "Finalizado backup do dia"


