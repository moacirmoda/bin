#!/usr/bin/env python

import os, json, re

handle = open('mapping_dirs.json')
dirs = json.load(handle)
handle.close()
count = 0
for dir in dirs:
	album = dir['dirname'].split('-')
	if len(album) > 1:
		dirs[count]['album'] = album[1].strip()
	count += 1

print json.dumps(dirs, indent=2)