#!/bin/bash

if [ "$#" == "0"  ]; then exit; fi;

ssh="ssh $1"
date=`date +%Y%m%d%H%M%S`

echo "Acessando servidor e fazendo backup.."
$ssh tar cvfzp /tmp/$1.tar.gz .

echo "Trazendo o backup para a máquina.."
scp -r $1:/tmp/$1.tar.gz /home/moa/backup/mexx/ 


