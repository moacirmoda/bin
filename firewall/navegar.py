#!/usr/bin/env python
#! coding: utf-8

from bs4 import BeautifulSoup
import requests, os, json, sys

def log(string):
    sys.stdout.write('ok')
    print 

def write_url_new(url):
    command = 'echo "%s" >> /tmp/new'
    res = os.system(command % url)

    if res > 0:
        return False
    return True

def write_url_navigated(url):
    command = 'echo "%s" >> /tmp/navigated'
    res = os.system(command % url)

    if res > 0:
        return False
    return True


def is_navigated(url):
    file = "/tmp/navigated"

    if os.path.exists(file):
        with open(file) as handle:
            data = handle.read()

        if url in data:
            return True
    return False


def navigate(url):


    prefix = "http://stackoverflow.com/"
    if is_navigated(url):
        return False
    
    s = requests.session()
    s.headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"
    try:
        r = s.get(url)
    except:
        return False

    for line in r.text.split("\n"):
        if '<a href="/questions/' in line:
            line = line.split("<a href=\"")[1]
            line = line.split('"')[0]
            
            new_url = prefix + line
            write_url_new(new_url)
    
first_url = "http://stackoverflow.com/questions/3226596/full-text-search-whoosh-vs-solr"
navigate(first_url)

count = 0
while True:
    handle = open("/tmp/new")
    for url in handle:
        url = url.strip()
        navigate(url)
        write_url_navigated(url)
        count += 1

        if count % 10 == 0 and count != 0:
            print count

