#!coding: utf-8
import os, sys, re, json
from eyed3 import mp3
from glob import glob
from pprint import pprint

DIR = '/home/moa/Música/Álbuns Completos'
REGEX_TO_CLEAN = (
    '(www\..*\.com)',
    '(www\..*\.net)',
    '(www\..*\.br)',
    '(www\..*\.org)',
)

print "Removendo sites dos títulos.."
count = 0
for root, dirs, files in os.walk(DIR):

    for regex in REGEX_TO_CLEAN:
        match = re.search(regex, root, re.IGNORECASE)
        if match:
            match = match.group()
            new_root = root.replace(match, "").strip()

            if len(new_root.split('-')[-1]) == 0:
                new_root = new_root[0:-1]

            pathname = new_root.split("/")[-1]
            new_root = new_root.replace(pathname, pathname.title())
            
            print "src: %s" % root
            print "tgt: %s" % new_root
            print 

            raw_input()
            os.rename(root, new_root)
            count += 1
    
print "%s alterados." % count
