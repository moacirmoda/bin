mkdir -p /tmp/key/
touch /tmp/key/authorized_keys 
scp -r ${1}:~/.ssh/authorized_keys /tmp/key
cat ~/.ssh/id_rsa.pub >> /tmp/key/authorized_keys

ssh ${1} mkdir -p .ssh
scp -r /tmp/key/authorized_keys ${1}:~/.ssh/

ssh ${1} "chmod 700 ~/.ssh"
ssh ${1} "chmod 600 ~/.ssh/authorized_keys"

#scp -r /home/moa/bin/.bashrc_server ${1}:~/ 
#ssh ${1} rm .bashrc
#ssh ${1} mv .bashrc_server .bashrc

#rm -r /tmp/key
