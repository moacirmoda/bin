#!/bin/bash

BKP_DIR=/mnt/DADOS/Backup/project
BKP_SRC=/home/moa/project
NOW=$(date +"%Y-%m-%d")
FILE=$BKP_DIR/project-files-$NOW.tgz
BD_FILE=$BKP_SRC/mysql-$NOW.sql

# backup de toda a base mysql
mysqldump -u root -pmoa00moa --all-databases > $BD_FILE

# compressao do diretório project
cd $BKP_SRC
tar cfzp $FILE .

# # enviando para o cloud
# scp -r $FILE crisfoto:$BKP_DIR

# # removendo os arquivos que foram criados localmente
# rm $FILE
rm $BD_FILE