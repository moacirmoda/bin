#!/bin/bash

file=$1
originalx=$2
originaly=$3
slicex=$4
slicey=$5
numberx=$((originalx/slicex))
numbery=$((originaly/slicey))

for i in `seq 0 $numberx`; do
  for j in `seq 0 $numbery`; do
    convert -crop ${slicex}x${slicey}+$((i*slicex))+$((j*slicey)) \
        $file $file-$j-$i.png
  done
done
