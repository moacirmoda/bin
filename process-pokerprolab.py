#!coding:utf-8
from BeautifulSoup import BeautifulSoup as Soup
import requests, sys
from glob import glob
from datetime import datetime, date, timedelta

results = []
for file in glob("/tmp/*.html"):
	with open(file) as handle:
		data = handle.read()
	soup = Soup(data)

	for table in soup.findAll('table', attrs={'id': 'ltControl'}):
		for tr in table.findAll('tr'):
			tds = tr.findAll('td')
			if len(tds) == 5:
				result = {}
				count = 0
				for td in tds:
					
					label = "date"
					if count == 1: label = 'title'
					if count == 2: label = 'buy-in'
					if count == 3: label = 'position'
					if count == 4: label = 'prize'

					result[label] = td.text
					count += 1

				if result['date'] == "Finalizado":
					continue

				# transformando prize e buy-in em float
				result['prize'] = float(result['prize'].replace("$", ''))
				result['buy-in'] = result['buy-in'].replace("$", '')
				result['buy-in'] = sum((float(a) for a in result['buy-in'].split("+")))

				# criando variável itm
				result['itm'] = False
				if result['prize'] > 0:
					result['itm'] = True

				# transformando data
				# result['date'] = datetime.strptime(result['date'], "%m/%d/%Y")

				results.append(result)

# import json
# print json.dumps(results, indent=2)

today = date.today()
total_days = today - date(year=2014, month=01, day=01)

total = len(results)
print "Total no ano: %s" % total
print "%s Games/dia" % round(float(total) / float(total_days.days), 2)

months = {}
total_itms = 0
buy_in = 0
prize = 0
for result in results:

	if result['itm'] == True:
		total_itms += 1

	buy_in += result['buy-in']
	prize += result['prize']

print "ITM: %s" % (total_itms*100/total) + "%"
print "Investimento: $%s" % buy_in
print "Retorno Bruto: $%s" % prize
print "Profit: $%s" % (prize - buy_in)

