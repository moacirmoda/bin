#!/usr/bin/env python
#!coding: utf-8

import os, sys, shutil 
import time as lib_time
sys.path.append('/home/moa/Dropbox/bin')

import EXIF

CAMERA_UPLOADS = "/home/moa/Dropbox/Camera Uploads"
IMAGE_PATH = "/home/moa/Dropbox/Imagens/Celular"
VIDEO_PATH = "/home/moa/Dropbox/Imagens/Vídeos"

MONTHS = {
	'01': u'Janeiro', 
	'02': u'Fevereiro', 
	'03': u'Março', 
	'04': u'Abril', 
	'05': u'Maio', 
	'06': u'Junho', 
	'07': u'Julho', 
	'08': u'Agosto', 
	'09': u'Setembro', 
	'10': u'Outubro', 
	'11': u'Novembro', 
	'12': u'Dezembro', 
}

VIDEO_EXTENSIONS = [
	'mp4',
]

try:
	os.mkdir("/tmp/photo", 0755)
except:
	pass

processed = 0
for root, path, files in os.walk(CAMERA_UPLOADS):
	for file in files:
		filename = file
		file = os.path.join(root, file)

		# check if file is a video
		ext = filename.split(".")[-1]

		if ext in VIDEO_EXTENSIONS:
			shutil.copyfile(file, os.path.join(VIDEO_PATH, filename))
			shutil.move(file, "/tmp/photo")
			processed += 1
			continue
		

		with open(file, 'rb') as handle:
			tags = EXIF.process_file(handle)

		already_has_info = False
		if not 'Image DateTime' in tags.keys():
			
			filename = file.split("/")[-1]
			try:
				date = filename.split(" ")[0]
				time = filename.split(" ")[1]
				year, month, day = date.split("-")
				hour, minute, second, ext = time.split(".")
				already_has_info = True

			except Exception, e:
				print "Skipping %s" % file
				continue

		if not already_has_info:
			datetime = unicode(tags['Image DateTime'])
			date, time = datetime.split(" ")
			year, month, day = date.split(":")
			hour, minute, second = time.split(":")

		month = "%s-%s" % (month, MONTHS[month])

		path_year = os.path.join(IMAGE_PATH, year)
		path_month = os.path.join(path_year, month)
		
		try:
			os.mkdir(path_year, 0755)
		except Exception, e:
			pass
		try:
			os.mkdir(path_month, 0755)
		except Exception, e:
			pass

		destiny = os.path.join(path_month, filename)
		if not os.path.exists(destiny):
			shutil.copyfile(file, destiny)
			processed += 1
		else:
			shutil.move(file, "/tmp/photo")


print "Backuping files.."
now = lib_time.strftime("%Y%m%d%H%M%S")
os.system("tar cvfzp /home/moa/bkp/bkp-camera-upload%s-.tar.gz /tmp/photo" % now)
os.system("rm -rf /tmp/photo")

print "Processed: %s" % processed
