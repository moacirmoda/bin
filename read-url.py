#!coding:utf-8
from BeautifulSoup import BeautifulSoup as Soup
import requests, sys

url = "http://www.uol.com.br"
if len(sys.argv) > 1:
	url = sys.argv[1]

resp = requests.get(url)
if resp.status_code != 200:
	print "Não foi possível carregar"
	sys.exit(1)

soup = Soup(resp.text)