#!coding:utf-8
import os
import sys
import requests

class CheckURL:

	def __init__(self):
		pass

	def get_list(self):

		try:
			file = sys.argv[1]
			with open(file) as handle:
				return handle.read().split()

		except IndexError:
			print "ERRO: Lista de URLs não indicada."
			sys.exit(1)
		
		except IOError:
			print "ERRO: Arquivo não existe."
			sys.exit(1)

	def process(self):

		map = {}
		for url in self.get_list():

			req = requests.get(url)
			print "%s\t\t%s" % (req.status_code, url)

			if req.status_code != 200:
				map[url] = req.status_code

		if map:
			print "\nERROS:"
			for key, value in map.items():
				print "%s\t\t%s" % (value, key)
		

if __name__ == '__main__':

	obj = CheckURL()
	obj.process()
