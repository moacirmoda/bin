#!/usr/bin/env python
#!coding:utf-8
import os
import sys
import json
import requests

if len(sys.argv) < 3:
    print 'Usage: ./download-ubook.py <har_filepath> <init_count>'
    sys.exit()

file = sys.argv[1]

with open(file) as handle:
    data = json.load(handle)

count = 0
urls = []
for entry in data['log']['entries']:
    if entry['request']['url'].endswith(".mp3"):
        count += 1
        urls.append(entry['request']['url'])

count = int(sys.argv[2])
for url in urls:
    
    print "%02d\t%s" % (count, url)

    req = requests.get(url)
    with open('/tmp/%02d.mp3' % count, 'w') as output:
        output.write(req.content)

    count += 1
