#!/usr/bin/python
#!coding: utf-8

import ftplib
import sys

if 'help' in sys.argv:
	print "Usage: "
	print "./send-via-ftp.py <hostname> <username> <password> <filepath> <destination_directory>"
	sys.exit()


if __name__ == '__main__':


	scriptname, hostname, username, password, filepath, dest = sys.argv

	filename = filepath.split('/')[-1]

	try:
		session = ftplib.FTP(hostname, username, password)
		file = open(filepath ,'rb')                  # file to send
		session.storbinary('STOR %s/%s' % (dest, filename), file)     # send the file
		file.close()                                    # close file and FTP
		session.quit()
		
	except Exception, e:
		print "ERRO: %s" % e