rm -rf /tmp/$2

echo Trazendo do servidor de dev
scp -r arcondev:~/git/$2 /tmp > /dev/null 2>&1  
echo Levando para o servidor $1
scp -r /tmp/$2/* $1:/opt/$2/ > /dev/null 2>&1

rm -rf /tmp/$2