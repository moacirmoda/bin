#!coding:utf-8

import os
from datetime import datetime

PATH = "D:\PokerStars\HandHistory\moamoda"

results = {}

proccess = 0
for root, dirs, files in os.walk(PATH):
	for file in files:
		path = os.path.join(root, file)
		
		buyin = path.replace(".txt", '').split()
		buyin = float(buyin[-3].replace("$", '').replace(',','.')) + float(buyin[-1].replace("$", '').replace(',','.'))
		
		date = None
		position = None
		prize = 0
		itm = False
		find = False
		for line in open(path):

			if not date:
				proccess += 1
				date = line
				date = date.split("-")[-1].strip()
				date = date.split(" ")[0]
				date = datetime.strptime(date, '%Y/%m/%d')

				if not date in results.keys():
					results[date] = {'ITM': 0, 'Buy-in': 0, 'Prize': 0, 'Total Games': 0}

			for phrase in ['moa.moda finished the tournament in ', 'moa.moda Acabou o torneio em ', 'moa.moda terminou o torneio em', 'moa.moda ganhou o torneio ', 'moa.moda won']:
				if phrase in line:
					position = line.replace(phrase, "").strip()

					for phrase2 in ['and received', 'e recebeu']:
						if phrase2 in position:
							position = position.replace(" - parabéns!", '')
							position = position.replace(" - congratulations!", '')
							prize = float(position.split(phrase2)[-1].strip()[1:-1])
							itm = True

					position = position.split()[0]

					results[date]['Buy-in'] += buyin
					results[date]['Prize'] += prize
					results[date]['Total Games'] += 1
					if itm:
						results[date]['ITM'] += 1
					find = True
		if not find:
			print file

total_buyin = 0
total_prize = 0
total_lucro = 0
total_jogos = 0
total_itm = 0

print "Data\t\tBuy-in\tPrize\tLucro\tJogos\tTotal ITM"
print '---------------------------------------------------------'
for key in sorted(results.keys()):
	item = results[key]
	lucro = item['Prize'] - item['Buy-in']
	print '%s\t$%.2f\t$%.2f\t$%.2f\t%s\t%s' % (str(key).split()[0], item['Buy-in'], item['Prize'], lucro, item['Total Games'], item['ITM'])

	total_buyin += results[key]['Buy-in']
	total_prize += results[key]['Prize']
	total_lucro += lucro
	total_jogos += results[key]['Total Games']
	total_itm += results[key]['ITM']
	
	
print '---------------------------------------------------------'
print '\t\t$%.2f\t$%.2f\t$%.2f\t%s\t%s' % (total_buyin, total_prize, total_lucro, total_jogos, total_itm)

raw_input()
