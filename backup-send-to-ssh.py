#!/usr/bin/env python
import os, sys
from datetime import datetime, date

if len(sys.argv) != 3:
	print "Usage: ./backup-send-to-ssh.py <path> <server>"
	sys.exit(0)

script, path, ssh = sys.argv

today = date.today()

bkp_filename = "%s-%s.tar.gz" % (path.replace("/", "_"), today)


