#!/usr/bin/env python
#!coding: utf-8

import os, sys

if len(sys.argv) != 4:
	print 'usage: ./search-replace.py <search> <replace> <file>'
	sys.exit()

search = sys.argv[1]
replace = sys.argv[2]
file = sys.argv[3]

newfile = ""
count = 0
for line in open(file):
	if search in line:
		line = line.replace(search, replace)
		count += 1
	newfile += line

if count > 0:
	with open(file, 'wb') as output:
		output.write(newfile)

print "Replaces: %s\t\t%s" % (count, file)