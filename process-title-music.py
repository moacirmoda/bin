#!coding: utf-8

import json, os, re
from glob import glob

DIR = '/home/moa/Música/Álbuns Completos'

with open(os.path.join(DIR, '.meta')) as handle:
	data = json.load(handle)

genres = {}
for item in data:
	
	genre = item['genre']
	path = item['path']
	cd = item['title']
	artist = item['artist'].lower().replace(" e ", " & ")

	item.pop('genre', None)

	print path
	for music in item['musics']:
		
		if os.path.isdir(music):
			print ".",
			continue
		
		music = music.replace(path + "/", "").replace(" e ", " & ").lower()
		
		if artist in music:
			music = music.replace(artist, "") # remove artist name
			music = music.replace('-', '') # remove -
			music = re.sub(' +',' ', music) # remove multiple spaces

			for number in re.findall(r'\b\d+\b', music):
				# removing year				
				if len(number) == 4:
					music = music.replace(number, "")
				
				# adding 0 when only has 1 digit
				if len(number) == 1:
					music = music.replace(number, "0%s. " % number)

				if len(number) == 2:
					music = music.replace(number, "%s. " % number)


			music = music.title()
			music = music.replace(" E ", " e ").replace(" Da ", " da ").replace(" Do ", " do ").replace(u" É ", u" é ")
		print music
	print 

		

	if genre in genres.keys():
		genres[genre].append(item)
	else:
		genres[genre] = [item]

# print json.dumps(genres, indent=2)
