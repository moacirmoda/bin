#coding: utf-8

import os
import sys
import glob
from datetime import datetime

prefix = "[%s] " % datetime.now() 
msg = prefix + "%s"

print msg % "Iniciando script"

if sys.argv > 1:
	
	range_file = sys.argv[1]

	for file in glob.glob(range_file):

		dir, filename = os.path.split(file)

		filename = filename.lower().replace(' ', '_')

		new_file = os.path.join(dir, filename)
		os.rename(file, new_file)
		print msg % "%s renomeado para %s" % (file, new_file)



