#!/bin/bash

SITES_AVAILABLE='/etc/apache2/sites-available'
TEMPLATE="${SITES_AVAILABLE}/template.conf"
PROJECT="/home/moa/project"

if [ $# == 0 ]
then
	echo 'ERRO: Informe o nome da instância!'
	exit 1;
fi

name=${1}
domain=www.${name}.dev
conf=${SITES_AVAILABLE}/${name}.conf

document_root="${PROJECT}/${name}/htdocs"

echo "* criando diretório da instância: '${document_root}'"
mkdir -p ${document_root}

cd ${SITES_AVAILABLE}

echo "* criando arquivo de vhost do apache: '${conf}'"
sudo cp ${TEMPLATE} ${conf}
sudo sed -i "s/template/${name}/g" ${conf}
sudo ln -s ${conf} /etc/apache2/sites-enabled
sudo service apache2 restart

# creating vhost
echo "* criando domínio no arquivo hosts: '${domain}'"
sudo echo "127.0.0.1 ${domain}" >> /etc/hosts

# creating database
echo "* criando base de dados mysql: '$1'"
/home/moa/bin/make-database.sh $1 
