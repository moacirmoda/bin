#!/bin/bash

BKP_DIR=/mnt/DADOS/Backup/Dropbox
BKP_SRC=/home/moa/Dropbox
NOW=$(date +"%Y-%m-%d")
FILE=$BKP_DIR/dropbox-files-$NOW.tgz

# compressao do diretório project
cd $BKP_SRC
tar cfzp $FILE .

# # enviando para o cloud
# scp -r $FILE crisfoto:$BKP_DIR

# # removendo os arquivos que foram criados localmente
# rm $FILE