#!coding:utf-8
import json
import os
import sys

file = sys.argv[1]
base = sys.argv[2] 

base_dir = "%s/site/" % os.path.dirname(file)

# criando o diretorio do site
os.system("mkdir -p %s" % base_dir)

with open(file) as handle:
    data = json.load(handle)

for item in data['log']['entries']:
    
    # voltando ao diretorio original
    os.chdir("%s" % base_dir)

    # pegando o file
    file = item['request']['url']

    # pegando somente os files internos
    if base in file:

        # removendo a base
        url = file
        file = file.replace(base, "")
        
        # pegando o diretorio
        dir = os.path.dirname(file)

        # limpando a string do diretorio
        if dir.startswith("/"):
            dir = dir[1:]

        # criando o diretorio caso n exista
        os.system("mkdir -p %s" % dir)

        if dir:
            os.chdir("%s" % dir)
        
        os.system("wget %s" % url)
        print url

