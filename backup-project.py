#!/bin/bash
#!coding:utf-8

#--------------------------------------------------------------------------------------------#
# Backup.py																					 #
#--------------------------------------------------------------------------------------------#
# Realiza backup do diretório project 														 #
# Data: 11/05/2015																			 #
#--------------------------------------------------------------------------------------------#

import os, sys, syslog
from datetime import date, timedelta
from time import time
import json

def debug(msg, exit=False):
	syslog.syslog(msg)
	print msg
	if exit:
		sys.exit(1)

PATH="/home/moa/project"
DEST="/media/moa/Dados/Backup"

CONFIG_PATH="/home/moa/.bkp"
CONFIG_FILE="%s/backup-project-config.json" % CONFIG_PATH

now = date.today()
prefix = "%s-%02d-%02d-project.tar.gz"

# carrega o arquivo de configuração
try:
	with open(CONFIG_FILE) as handle:
		data = json.loads(handle.read())
except Exception as e:
	data = {
		'last_modified': time(),
	}

data['last_modified'] = time() - 60*60*24*6
last_modified = date.fromtimestamp(data['last_modified'])

debug("Última execução: %s" % last_modified)

# se o último backup tiver mais de 5 dias, faz um novo
if last_modified + timedelta(days=5) < now:

	# fazendo backup de hoje
	cmd = "tar cfzp %s/%s %s" % (DEST, prefix % (now.year, now.month, now.day), PATH)
	debug("Criando: %s" % cmd)
	res = os.system(cmd)
	if res > 0:
		debug("Erro ao realizar backup, saindo..", True)

	# atualiza o arquivo de config
	data['last_modified'] = time()-60*60

# #removendo semana antiga	
# current_time = now - timedelta(weeks=3)
# current_path = "%s/%s" % (DEST, prefix % (current_time.year, current_time.month, current_time.day))
# if os.path.exists(current_path):
# 	cmd = "rm -rf %s" % current_path
# 	res = os.system(cmd)
# 	if res > 0:
# 		debug("Erro ao remover backups antigos..", True)

# salva o arquivo de config
with open(CONFIG_FILE, 'w') as output:
	output.write(json.dumps(data, indent=4))