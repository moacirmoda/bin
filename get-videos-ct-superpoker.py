#! coding:utf-8
from bs4 import BeautifulSoup
import requests, os, json

login = "moa.moda@gmail.com"
senha = "moa00moa"

s = requests.session()
s.headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"

# login
url = 'http://www.ctsuperpoker.com/login.php'
credenciais = {'login': login, 'senha': senha}
a = s.post(url, data=credenciais)

# videos
videos = []

file = '/tmp/data.json'
if os.path.exists(file):
	with open(file) as handle:
		final_videos = json.load(handle)

else:
	for nivel in range(1,4):

		end = True
		page = 1
		while True:	
			data = (nivel, page, (page * 5) -5)
			url = 'http://ctsuperpoker.com/videos.php?nivel=%s&pagina=%s&limite_inicio=%s' % data
			r = s.get(url, allow_redirects=False)

			soup = BeautifulSoup(r.text)
			for a in soup.findAll('a'):
			    if a['href'].startswith('video-d'):
			    	video = (a['title'], a['href'])
			    	videos.append(video)
			    	end = False

			if end: break
			page += 1
			end = True

	print "Total: %s" % len(videos)
	niveis = ['Básico', 'Intermediário', 'Avançado']

	final_videos = []
	count = -1
	for title, link in videos:
		count += 1

		video = {
			'page': link,
			'title': title,
		}

		args = link.split("?")[1]
		args = args.split("&")
		for arg in args:
			key, value = arg.split("=")
			if key == 'nivel': value = niveis[int(value)-1]
			video[key] = value

		video['link'] = "http://www.ctsuperpoker.com/videos/%04d.mp4" % int(video['video'])
		final_videos.append(video)

	with open(file, 'w') as output:
		json.dump(final_videos, output)

res = 0
for video in final_videos:

	print "Baixando %s.. " % video['title']
	# res += os.system("wget %s" % video['link'])

	# file = "/tmp/%04d.mp4" % int(video['video'])
	# if os.path.exists(file):
	# 	new_name = "/tmp/%s.mp4" % video['title']
	# 	os.rename(file, new_name)
