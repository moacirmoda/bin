#!/usr/bin/env python
#!coding: utf-8

import os, sys
from datetime import datetime, timedelta

def execute(string, exit=True):
	res = os.system(string)
	if exit and res > 0:
		print "ERROR"
		sys.exit(1)

bkp_dir = "/home/moa/bkp"
now = datetime.now()

for i in range(1,4):
	last = now - timedelta(days=i)
	string = "mv %s/*%d%02d%02d* /tmp" % (bkp_dir, last.year, last.month, last.day)
	execute(string, False)

string = "rm -rfv %s/*" % bkp_dir


string = "rm -rfv %s/*" % bkp_dir
execute(string)

string = "mv /tmp/*.tar.gz %s/" % bkp_dir
execute(string)