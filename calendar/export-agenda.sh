
YEAR=`date '+%Y'`
MONTH=`date '+%m'`
DAY=`date '+%d'`

file="webcalendar.txt"

# normalizando o mês para um caractere só, caso seja assim
if [ `echo $MONTH | cut -c 1` -eq "0" ]; then
    MONTH=`echo $MONTH | cut -c 2`
fi

if [ $MONTH -eq "12" ]; then
    YEAR=$(($YEAR+1))
    MONTH=1
else
    MONTH=$(($MONTH+1))
fi

# faz o login no sistema e salva um cookie
curl -c cookie.txt -d "login=moacir.moda&password=moa00moa00" http://agenda.bireme.org/login.php

# baixa o calendário inteiro
curl -b cookie.txt -d "format=ical&cat_filter=&fromday=18&frommonth=2&fromyear=2013&endday=30&endmonth=$MONTH&endyear=$YEAR&modday=01&modmonth=1&modyear=2013" http://agenda.bireme.org/export_handler.php > $file

python import-to-google.py $file

rm $file cookie.txt