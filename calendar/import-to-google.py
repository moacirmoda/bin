#!/usr/bin/env python
#! coding: utf-8

from icalendar import Calendar, Event
import unidecode
import sys
import re
import os

database = 'database.txt'
row_format = "[%s] %s\n"
google_row = "google calendar add '%s %s %s'"

if len(sys.argv) != 2:
    raise SystemExit

with open(database, 'a') as handle:
    pass

db_content = ""
with open(database) as handle:
    db_content = handle.read()

# content = {}
# for calendar in calendars:
#     with open(calendar) as handle:
#         content[calendar] = handle.read()

# output = ""
# for calendar in calendars:
#     with open(calendar) as handle:
#         linecount = 0
#         for line in handle:

#             # pulando os cabecalhos
#             if line > 22:
#                 for file in content.keys():
#                     if file != calendar:
#                         if not line in content[file]:
#                             output += line
#             linecount += 1

# print output


def slugify(str):
    return unidecode.unidecode(str)

cal = Calendar.from_ical(open(sys.argv[1],'rb').read())

for event in cal.walk():
    if type(event) == type(Event()):
        
        subject = slugify(event.get('SUMMARY'))
        date = event.get('DTSTART').dt
        row = row_format % (date.isoformat(), subject[:40])

        if row in db_content:
            continue 

        # se for horario de verão, é -2
        hour = date.hour - 3
        if hour >= 12:
            hour = hour - 12
            
            if date.minute != 0:
                hour = "%i:%ipm" % (hour, date.minute)
            else:
                hour = "%ipm" % hour

        else:
            if date.minute != 0:
                hour = "%i:%iam" % (hour, date.minute)
            else:
                hour = "%iam" % hour


        month = str(date.month)
        if date.month < 10:
            month = "0%i" % date.month

        date = "%s-%s-%s" % (date.day, month, str(date.year)[2:])

        try:
            command = google_row % (subject, date, hour)
            os.system(command)
            print command

            with open(database, 'a') as handle:
                handle.write(row)
        except:
            print 'Error'


        
