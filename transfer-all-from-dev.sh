declare -a arr=(arcon-auth arcon-cli arcon-dpi arcon-qos arcon-vpn arcon-database arcon-firewall arcon-proxy-http arcon-snmp arcon-wop)

for i in ${arr[@]}
do
	echo "Transfering '$i' to $1.."
	/home/moa/bin/transfer-from-dev.sh $1 $i > /dev/null 2> /dev/null
done

echo "Transfering arcon-base to $1.."
/home/moa/bin/transfer-from-dev-arcon-base.sh $1 > /dev/null 2> /dev/null

echo "Transfering interface to $1.."
/home/moa/bin/transfer-from-dev-interface.sh $1 > /dev/null 2> /dev/null
