#!coding: utf-8
import os, sys, re, json
from eyed3 import mp3
from glob import glob
from pprint import pprint

DIR = '/home/moa/Música/Álbuns Completos'

cds = []
for path in glob(DIR + "/*/*"):

    cd = path.split("/")[-1]
    genre = path.replace("/" + cd, "").split("/")[-1].replace('-', '').title()

    if "Discografia" in cd:
        continue

    cd = cd.split("-")
    if len(cd) <= 1:
        continue

    artist = cd[0].strip().title()
    title = cd[1].strip().title()

    try:
        year = int(cd[2].strip())
    except:
        year = None

    current = {
        'title': title,
        'artist': artist,
        'genre': genre,
        'path': path,
        'musics': glob(path + "/*")
    }
    if year:
        current['year'] = year

    cds.append(current)

with open(os.path.join(DIR, '.meta'), 'w') as output:
    json.dump(cds, output, indent=2)



        # if mp3.isMp3File(file):
        #     _file = mp3.Mp3AudioFile(file)

        #     # tag infos
        #     try:
        #         artist = _file.tag.artist
        #         title = _file.tag.title
        #         track, total_tracks = _file.tag.track_num
        #         album = _file.tag.album
        #     except:
        #         print "ERROR: %s" % file
        #         continue



