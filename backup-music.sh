#!/bin/bash

bkpname=/tmp/musicas.tar.gz
dir='/home/moa/Música/albuns/'
bkpdestiny='/media/win7/backup/'

cd $dir

tar cvfzp $bkpname *

cp $bkpname $bkpdestiny

rm $bkpname
