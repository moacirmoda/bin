rm -rf /tmp/arcon-base

echo Trazendo arcon-base do servidor $1 
scp -r $1:/opt/arcon-base/ /tmp > /dev/null

rm -rf /tmp/arcon-base/interface
rm -rf /tmp/arcon-base/bin
rm -rf conf/.popdb
find /tmp/arcon-base -type f -name "*.plc" | xargs rm -rf

echo Levando para o servidor dev 
scp -r /tmp/arcon-base/* arcondev:~/git/arcon-base/ > /dev/null 

rm -rf /tmp/arcon-base