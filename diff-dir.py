#!/usr/bin/env python
#!coding: utf-8

import os
import sys
from glob import glob

if len(sys.argv) > 2:

	path1 = sys.argv[1]
	path2 = sys.argv[2]

	p1_list = []
	for root, dirs, files in os.walk(path1):
		p1_list.append(root.replace(path1, ""))

	p2_list = []
	for root, dirs, files in os.walk(path2):
		p2_list.append(root.replace(path2, ""))

	print '* Comparando..'
	print ''

	print "# Diretórios que não existem em %s: \n" % path2
	for item in p1_list:

		if item not in p2_list:
			print '- %s ' % item
		else:
			p1_files = glob("%s/%s/*.*" % (path1, item))
			p2_files = glob("%s/%s/*.*" % (path2, item))

			p1 = []
			for file in p1_files:
				p1.append(file.replace(path1, ""))
			
			p2 = []
			for file in p2_files:
				p2.append(file.replace(path2, ""))

			for file in p1:
				if file not in p2:
					print '- %s ' % file

	print ""
	print "# Diretórios que não existem em %s: \n" % path1
	for item in p2_list:

		if item not in p1_list:
			print '- %s ' % item
		else:
			p1_files = glob("%s/%s/*.*" % (path1, item))
			p2_files = glob("%s/%s/*.*" % (path2, item))

			p1 = []
			for file in p1_files:
				p1.append(file.replace(path1, ""))
			
			p2 = []
			for file in p2_files:
				p2.append(file.replace(path2, ""))

			for file in p2:
				if file not in p1:
					print '- %s ' % file
			


else:
	print "\nERRO: Insira dois diretórios para serem comparados..\n"
