#!/bin/bash

# Script que atualiza todos os repositórios svn da bireme
# Moacir Moda Neto - 29/09/2011

CUR_DIR=`pwd`

PROJECT_DIR="/home/moa/project/bireme/svn"
TMP_DIR="/tmp"

ls ${PROJECT_DIR} >> ${TMP_DIR}/ls.ls

cd ${PROJECT_DIR}
while read line
do
	echo "* Updating project ${line}.."
	cd ${line}
	svn update 1> /dev/null 2> /dev/null
	cd ..

done < ${TMP_DIR}/ls.ls

cd ${CUR_DIR}
rm ${TMP_DIR}/ls.ls

